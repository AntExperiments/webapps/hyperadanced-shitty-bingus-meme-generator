let memes = [
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/b9278ef0-a299-42b1-aad7-0548c9b212da.jpg", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/3dc5d0cd-1524-4fe7-bc43-d006841bc62d.jpg", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/626b6de8-0a2b-476a-8a27-10593ae026f3.png", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/ddcbd8a3-0199-42ec-8c51-b461a9d28860.jpg", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/63134ecf-3d39-4d04-b761-067080eba879.jpg", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/7b63cb9b-a46f-4fe6-b39f-45016556ed2e.png", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/769ed15b-0938-4ac0-9c45-d2d961814735.jpg", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/babcff20-517c-4dbd-98f2-e9a990d2674a.jpg", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/06f01f61-11fc-409a-96c2-93cbe115f24b.jpg", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/e080f916-f109-4f2e-8e44-bfd883b4c1ca.jpg", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/071daeb1-8e44-4c06-ad04-0941bef61ef5.png", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/c4bc2871-7563-48a0-8c6f-42f533b45e14.png", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/19e58dc5-9161-46d9-892f-d1ad8e5e30c6.png", 
    "https://assets.editor.p5js.org/6021689232b9d40024c8dedf/b1502969-c69c-47a7-a1aa-c342213625a7.gif"
];

let annotations = [
    "swag",
    "what a dapper fella",
    "there is nothing I love more in this world",
    "vegetable",
    "protect me, maria",
    "heaven's light",
    "everything I do, I do it for you",
    "20 boxes of cerel",
    "PEACE AND LOVE",
    "what a lovely day bingus",
    "I like to move it move it",
    "all hail the king",
    "live a little",
    "instablox",
    "I'm not gonna write you a love song",
    "you are all I long for, all I worship and adore",
    "penguing",
    "the world is cruel",
    "undopromosyo",
    "mo yundo"
];

const divElement = document.querySelector('div');
const imgElement = document.querySelector('img');
const spanElement = document.querySelector('span');

const updateMeme = () => {
    // Always get random (but also new element)
    imgElement.src = memes.sort(() => Math.random() - 0.5).filter(i => i != imgElement.src)[0];
    spanElement.innerText = annotations.sort(() => Math.random() - 0.5).filter(i => i != spanElement.innerText)[0];

    // RIDICULOUS ANIMATIONS
    switch (Math.round(Math.random())) {
        case 1:
            divElement.classList.add('spin');
            setTimeout(() => divElement.classList.remove('spin'), 500);
            break;
    
        default:
            divElement.classList.add('spin2');
            setTimeout(() => divElement.classList.remove('spin2'), 500);
            break;
    }
};

document.querySelector('div').onclick = updateMeme;

updateMeme();